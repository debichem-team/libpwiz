#!/bin/sh

WORK_DIR=$PWD

DEBIAN_DIR=${WORK_DIR}/debian

BIN_DIR=${DEBIAN_DIR}/build/usr/bin

cd ${BIN_DIR}

for file in $(find -type f -executable)
do 
    chrpath -d ${file}
done
